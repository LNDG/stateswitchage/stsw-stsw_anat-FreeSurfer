% subcortical parcellation

% 171120 JQK | altered plot outputs, changed paths to new structure

%% extract data

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/';

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';...
    '1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

for indSub = 1:numel(IDs)
    disp(IDs{indSub});
    curFileName = [pn.data,IDs{indSub},'/stats/aseg.stats'];
    if exist(curFileName, 'file')
        subData = importdata(curFileName);
        for indRegion = 81:125 % these should always be in the same position; check!
            whitespace = regexp(subData{indRegion,1}, '\s');
            indices = whitespace(1+ find(diff(regexp(subData{indRegion,1}, '\s')) > 1)); % identify points at which whitespaces change
            for indEntry = 1:numel(indices)
                if indEntry ~= numel(indices)
                    cellstructure{indRegion-80,indEntry} = strtrim(subData{indRegion,1}(indices(indEntry):indices(indEntry+1)));
                else cellstructure{indRegion-80,indEntry} = strtrim(subData{indRegion,1}(indices(indEntry):end));
                end
            end
        end
        FreeSurferStatsS1.ID{indSub,1} = IDs{indSub}(end-3:end);
        FreeSurferStatsS1.Regions(:, indSub) = cellstructure(:,4);
        FreeSurferStatsS1.IntensityNormMean(:, indSub) = str2num(str2mat(cellstructure{:,5}));
        FreeSurferStatsS1.IntensityNormStdDev(:, indSub) = str2num(str2mat(cellstructure{:,6}));
        FreeSurferStatsS1.IntensityNormMin(:, indSub) = str2num(str2mat(cellstructure{:,7}));
        FreeSurferStatsS1.IntensityNormMax(:, indSub) = str2num(str2mat(cellstructure{:,8}));
        FreeSurferStatsS1.IntensityNormRange(:, indSub) = str2num(str2mat(cellstructure{:,9}));
        FreeSurferStatsS1.NVoxels(:, indSub) = str2num(str2mat(cellstructure{:,2}));
        FreeSurferStatsS1.Volume(:, indSub) = str2num(str2mat(cellstructure{:,3}));
    else disp(['Subject missing: ',IDs{indSub},'. Please check.']);
    end
end

pn.brewer   = '/Volumes/LNDG/Projects/SleepThal/C_data/A_thalamusFreeSurfer/D_tools/brewermap/'; addpath(pn.brewer);
cmap        = brewermap(2,'YlOrRd');

%% create figures

AgeCatList

h = figure;
subplot(2,3,1)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS1.IntensityNormMean(5,idxGroup);
        yvals = FreeSurferStatsS1.IntensityNormMean(23,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('lThalamus'); ylabel('rThalamus'); title('S1: Mean Intensity')
subplot(2,3,2)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS1.Volume(5,idxGroup);
        yvals = FreeSurferStatsS1.Volume(23,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('lThalamus'); ylabel('rThalamus'); title('S1: Volume')
subplot(2,3,3)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS1.IntensityNormMean(5,idxGroup);
        yvals = FreeSurferStatsS1.Volume(5,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('lThalamus'); ylabel('lThalamus'); title('S1: Intensity-Volume')
% session 2
subplot(2,3,4)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS2.IntensityNormMean(5,idxGroup);
        yvals = FreeSurferStatsS2.IntensityNormMean(23,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('lThalamus'); ylabel('rThalamus'); title('S2: Mean Intensity')
subplot(2,3,5)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS2.Volume(5,idxGroup);
        yvals = FreeSurferStatsS2.Volume(23,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('lThalamus'); ylabel('rThalamus'); title('S2: Volume')
subplot(2,3,6)
    for indGroup = 1:2
        idxGroup = find(AgeCatList(:,2)==indGroup);
        xvals = FreeSurferStatsS2.IntensityNormMean(23,idxGroup);
        yvals = FreeSurferStatsS2.Volume(23,idxGroup);
        deleteInd = find(xvals==0|yvals ==0);
        xvals(deleteInd) = []; yvals(deleteInd) = [];
        scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
    end
    set(gca,'color','black')
    xlabel('rThalamus'); ylabel('rThalamus'); title('S2: Intensity-Volume')

pn.plotFolder = '/Volumes/LNDG/Projects/SleepThal/C_data/A_thalamusFreeSurfer/C_figures/';
figureName = 'FreeSurfer_WithinSessionRelationships';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% behavioral associations

h = figure;
for indGroup = 1:2
    xvals = AgeCatList(:,3); % SQI
    yvals = FreeSurferStatsS2.Volume(5,:);
    scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
end

h = figure;
for indGroup = 1:2
    xvals = AgeCatList(:,4); % sleep need
    yvals = FreeSurferStatsS2.Volume(5,:);
    scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
end

h = figure;
for indGroup = 1:2
    xvals = AgeCatList(:,5); % BIS Attentional
    yvals = FreeSurferStatsS2.Volume(5,:);
    scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
end

h = figure;
for indGroup = 1:2
    xvals = AgeCatList(:,6); % HADS Anxiety
    yvals = FreeSurferStatsS2.Volume(5,:);
    scatter(xvals, yvals,'MarkerFaceColor', cmap(indGroup,:), 'MarkerEdgeColor',cmap(indGroup,:)); hold on;
end