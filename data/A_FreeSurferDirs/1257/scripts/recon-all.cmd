

#---------------------------------
# New invocation of recon-all Tue Apr 10 14:19:27 CEST 2018 

 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1257_T1w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig/001.mgz 

#--------------------------------------------
#@# T2/FLAIR Input Tue Apr 10 14:20:38 CEST 2018

 mri_convert --no_scale 1 /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1257_T2w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig/T2raw.mgz 

#--------------------------------------------
#@# MotionCor Tue Apr 10 14:21:55 CEST 2018

 cp /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig/001.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/rawavg.mgz 


 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/rawavg.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig.mgz --conform 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/talairach.xfm /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig.mgz 

#--------------------------------------------
#@# Talairach Tue Apr 10 14:23:24 CEST 2018

 mri_nu_correct.mni --no-rescale --i orig.mgz --o orig_nu.mgz --n 1 --proto-iters 1000 --distance 50 


 talairach_avi --i orig_nu.mgz --xfm transforms/talairach.auto.xfm 

talairach_avi log file is transforms/talairach_avi.log...

 cp transforms/talairach.auto.xfm transforms/talairach.xfm 

#--------------------------------------------
#@# Talairach Failure Detection Tue Apr 10 14:38:27 CEST 2018

 talairach_afd -T 0.005 -xfm transforms/talairach.xfm 


 awk -f /opt/freesurfer/6.0.0/bin/extract_talairach_avi_QA.awk /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/talairach_avi.log 


 tal_QC_AZS /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/talairach_avi.log 

#--------------------------------------------
#@# Nu Intensity Correction Tue Apr 10 14:38:30 CEST 2018

 mri_nu_correct.mni --i orig.mgz --o nu.mgz --uchar transforms/talairach.xfm --n 2 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/talairach.xfm nu.mgz nu.mgz 

#--------------------------------------------
#@# Intensity Normalization Tue Apr 10 14:51:54 CEST 2018

 mri_normalize -g 1 -mprage nu.mgz T1.mgz 

#--------------------------------------------
#@# Skull Stripping Tue Apr 10 14:53:23 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mri_em_register.skull.dat -skull nu.mgz /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta 


 mri_watershed -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mri_watershed.dat -T1 -brain_atlas /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta T1.mgz brainmask.auto.mgz 


 cp brainmask.auto.mgz brainmask.mgz 

#-------------------------------------
#@# EM Registration Tue Apr 10 14:58:33 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mri_em_register.dat -uns 3 -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta 

#--------------------------------------
#@# CA Normalize Tue Apr 10 15:02:14 CEST 2018

 mri_ca_normalize -c ctrl_pts.mgz -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta norm.mgz 

#--------------------------------------
#@# CA Reg Tue Apr 10 15:03:33 CEST 2018

 mri_ca_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mri_ca_register.dat -nobigventricles -T transforms/talairach.lta -align-after -mask brainmask.mgz norm.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.m3z 

#--------------------------------------
#@# SubCort Seg Tue Apr 10 16:39:16 CEST 2018

 mri_ca_label -relabel_unlikely 9 .3 -prior 0.5 -align norm.mgz transforms/talairach.m3z /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca aseg.auto_noCCseg.mgz 


 mri_cc -aseg aseg.auto_noCCseg.mgz -o aseg.auto.mgz -lta /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/cc_up.lta 1257 

#--------------------------------------
#@# Merge ASeg Tue Apr 10 17:21:05 CEST 2018

 cp aseg.auto.mgz aseg.presurf.mgz 

#--------------------------------------------
#@# Intensity Normalization2 Tue Apr 10 17:21:05 CEST 2018

 mri_normalize -mprage -aseg aseg.presurf.mgz -mask brainmask.mgz norm.mgz brain.mgz 

#--------------------------------------------
#@# Mask BFS Tue Apr 10 17:23:26 CEST 2018

 mri_mask -T 5 brain.mgz brainmask.mgz brain.finalsurfs.mgz 

#--------------------------------------------
#@# WM Segmentation Tue Apr 10 17:23:28 CEST 2018

 mri_segment -mprage brain.mgz wm.seg.mgz 


 mri_edit_wm_with_aseg -keep-in wm.seg.mgz brain.mgz aseg.presurf.mgz wm.asegedit.mgz 


 mri_pretess wm.asegedit.mgz wm norm.mgz wm.mgz 

#--------------------------------------------
#@# Fill Tue Apr 10 17:25:10 CEST 2018

 mri_fill -a ../scripts/ponscc.cut.log -xform transforms/talairach.lta -segmentation aseg.auto_noCCseg.mgz wm.mgz filled.mgz 

#--------------------------------------------
#@# Tessellate lh Tue Apr 10 17:25:44 CEST 2018

 mri_pretess ../mri/filled.mgz 255 ../mri/norm.mgz ../mri/filled-pretess255.mgz 


 mri_tessellate ../mri/filled-pretess255.mgz 255 ../surf/lh.orig.nofix 


 rm -f ../mri/filled-pretess255.mgz 


 mris_extract_main_component ../surf/lh.orig.nofix ../surf/lh.orig.nofix 

#--------------------------------------------
#@# Tessellate rh Tue Apr 10 17:25:49 CEST 2018

 mri_pretess ../mri/filled.mgz 127 ../mri/norm.mgz ../mri/filled-pretess127.mgz 


 mri_tessellate ../mri/filled-pretess127.mgz 127 ../surf/rh.orig.nofix 


 rm -f ../mri/filled-pretess127.mgz 


 mris_extract_main_component ../surf/rh.orig.nofix ../surf/rh.orig.nofix 

#--------------------------------------------
#@# Smooth1 lh Tue Apr 10 17:25:53 CEST 2018

 mris_smooth -nw -seed 1234 ../surf/lh.orig.nofix ../surf/lh.smoothwm.nofix 

#--------------------------------------------
#@# Smooth1 rh Tue Apr 10 17:25:54 CEST 2018

 mris_smooth -nw -seed 1234 ../surf/rh.orig.nofix ../surf/rh.smoothwm.nofix 

#--------------------------------------------
#@# Inflation1 lh Tue Apr 10 17:25:59 CEST 2018

 mris_inflate -no-save-sulc ../surf/lh.smoothwm.nofix ../surf/lh.inflated.nofix 

#--------------------------------------------
#@# Inflation1 rh Tue Apr 10 17:25:59 CEST 2018

 mris_inflate -no-save-sulc ../surf/rh.smoothwm.nofix ../surf/rh.inflated.nofix 

#--------------------------------------------
#@# QSphere lh Tue Apr 10 17:26:31 CEST 2018

 mris_sphere -q -seed 1234 ../surf/lh.inflated.nofix ../surf/lh.qsphere.nofix 

#--------------------------------------------
#@# QSphere rh Tue Apr 10 17:26:31 CEST 2018

 mris_sphere -q -seed 1234 ../surf/rh.inflated.nofix ../surf/rh.qsphere.nofix 

#--------------------------------------------
#@# Fix Topology Copy lh Tue Apr 10 17:29:49 CEST 2018

 cp ../surf/lh.orig.nofix ../surf/lh.orig 


 cp ../surf/lh.inflated.nofix ../surf/lh.inflated 

#--------------------------------------------
#@# Fix Topology Copy rh Tue Apr 10 17:29:49 CEST 2018

 cp ../surf/rh.orig.nofix ../surf/rh.orig 


 cp ../surf/rh.inflated.nofix ../surf/rh.inflated 

#@# Fix Topology lh Tue Apr 10 17:29:49 CEST 2018

 mris_fix_topology -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_fix_topology.lh.dat -mgz -sphere qsphere.nofix -ga -seed 1234 1257 lh 

#@# Fix Topology rh Tue Apr 10 17:29:49 CEST 2018

 mris_fix_topology -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_fix_topology.rh.dat -mgz -sphere qsphere.nofix -ga -seed 1234 1257 rh 


 mris_euler_number ../surf/lh.orig 


 mris_euler_number ../surf/rh.orig 


 mris_remove_intersection ../surf/lh.orig ../surf/lh.orig 


 rm ../surf/lh.inflated 


 mris_remove_intersection ../surf/rh.orig ../surf/rh.orig 


 rm ../surf/rh.inflated 

#--------------------------------------------
#@# Make White Surf lh Tue Apr 10 17:41:38 CEST 2018

 mris_make_surfaces -aseg ../mri/aseg.presurf -white white.preaparc -noaparc -whiteonly -mgz -T1 brain.finalsurfs 1257 lh 

#--------------------------------------------
#@# Make White Surf rh Tue Apr 10 17:41:38 CEST 2018

 mris_make_surfaces -aseg ../mri/aseg.presurf -white white.preaparc -noaparc -whiteonly -mgz -T1 brain.finalsurfs 1257 rh 

#--------------------------------------------
#@# Smooth2 lh Tue Apr 10 17:45:47 CEST 2018

 mris_smooth -n 3 -nw -seed 1234 ../surf/lh.white.preaparc ../surf/lh.smoothwm 

#--------------------------------------------
#@# Smooth2 rh Tue Apr 10 17:45:47 CEST 2018

 mris_smooth -n 3 -nw -seed 1234 ../surf/rh.white.preaparc ../surf/rh.smoothwm 

#--------------------------------------------
#@# Inflation2 lh Tue Apr 10 17:45:53 CEST 2018

 mris_inflate -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_inflate.lh.dat ../surf/lh.smoothwm ../surf/lh.inflated 

#--------------------------------------------
#@# Inflation2 rh Tue Apr 10 17:45:53 CEST 2018

 mris_inflate -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_inflate.rh.dat ../surf/rh.smoothwm ../surf/rh.inflated 

#--------------------------------------------
#@# Curv .H and .K lh Tue Apr 10 17:46:26 CEST 2018

 mris_curvature -w lh.white.preaparc 


 mris_curvature -thresh .999 -n -a 5 -w -distances 10 10 lh.inflated 

#--------------------------------------------
#@# Curv .H and .K rh Tue Apr 10 17:46:26 CEST 2018

 mris_curvature -w rh.white.preaparc 


 mris_curvature -thresh .999 -n -a 5 -w -distances 10 10 rh.inflated 


#-----------------------------------------
#@# Curvature Stats lh Tue Apr 10 17:47:26 CEST 2018

 mris_curvature_stats -m --writeCurvatureFiles -G -o ../stats/lh.curv.stats -F smoothwm 1257 lh curv sulc 


#-----------------------------------------
#@# Curvature Stats rh Tue Apr 10 17:47:30 CEST 2018

 mris_curvature_stats -m --writeCurvatureFiles -G -o ../stats/rh.curv.stats -F smoothwm 1257 rh curv sulc 

#--------------------------------------------
#@# Sphere lh Tue Apr 10 17:47:34 CEST 2018

 mris_sphere -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_sphere.lh.dat -seed 1234 ../surf/lh.inflated ../surf/lh.sphere 

#--------------------------------------------
#@# Sphere rh Tue Apr 10 17:47:34 CEST 2018

 mris_sphere -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_sphere.rh.dat -seed 1234 ../surf/rh.inflated ../surf/rh.sphere 

#--------------------------------------------
#@# Surf Reg lh Tue Apr 10 18:17:29 CEST 2018

 mris_register -curv -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_register.lh.dat ../surf/lh.sphere /opt/freesurfer/6.0.0/average/lh.folding.atlas.acfb40.noaparc.i12.2016-08-02.tif ../surf/lh.sphere.reg 

#--------------------------------------------
#@# Surf Reg rh Tue Apr 10 18:17:29 CEST 2018

 mris_register -curv -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/touch/rusage.mris_register.rh.dat ../surf/rh.sphere /opt/freesurfer/6.0.0/average/rh.folding.atlas.acfb40.noaparc.i12.2016-08-02.tif ../surf/rh.sphere.reg 

#--------------------------------------------
#@# Jacobian white lh Tue Apr 10 19:12:08 CEST 2018

 mris_jacobian ../surf/lh.white.preaparc ../surf/lh.sphere.reg ../surf/lh.jacobian_white 

#--------------------------------------------
#@# Jacobian white rh Tue Apr 10 19:12:08 CEST 2018

 mris_jacobian ../surf/rh.white.preaparc ../surf/rh.sphere.reg ../surf/rh.jacobian_white 

#--------------------------------------------
#@# AvgCurv lh Tue Apr 10 19:12:10 CEST 2018

 mrisp_paint -a 5 /opt/freesurfer/6.0.0/average/lh.folding.atlas.acfb40.noaparc.i12.2016-08-02.tif#6 ../surf/lh.sphere.reg ../surf/lh.avg_curv 

#--------------------------------------------
#@# AvgCurv rh Tue Apr 10 19:12:10 CEST 2018

 mrisp_paint -a 5 /opt/freesurfer/6.0.0/average/rh.folding.atlas.acfb40.noaparc.i12.2016-08-02.tif#6 ../surf/rh.sphere.reg ../surf/rh.avg_curv 

#-----------------------------------------
#@# Cortical Parc lh Tue Apr 10 19:12:12 CEST 2018

 mris_ca_label -l ../label/lh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 lh ../surf/lh.sphere.reg /opt/freesurfer/6.0.0/average/lh.DKaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/lh.aparc.annot 

#-----------------------------------------
#@# Cortical Parc rh Tue Apr 10 19:12:12 CEST 2018

 mris_ca_label -l ../label/rh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 rh ../surf/rh.sphere.reg /opt/freesurfer/6.0.0/average/rh.DKaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/rh.aparc.annot 

#--------------------------------------------
#@# Make Pial Surf lh Tue Apr 10 19:12:26 CEST 2018

 mris_make_surfaces -orig_white white.preaparc -orig_pial white.preaparc -aseg ../mri/aseg.presurf -mgz -T1 brain.finalsurfs 1257 lh 

#--------------------------------------------
#@# Make Pial Surf rh Tue Apr 10 19:12:26 CEST 2018

 mris_make_surfaces -orig_white white.preaparc -orig_pial white.preaparc -aseg ../mri/aseg.presurf -mgz -T1 brain.finalsurfs 1257 rh 

#--------------------------------------------
#@# Refine Pial Surfs w/ T2/FLAIR Tue Apr 10 19:25:32 CEST 2018

 bbregister --s 1257 --mov /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig/T2raw.mgz --lta /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/T2raw.auto.lta --init-coreg --T2 


 cp /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/T2raw.auto.lta /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/T2raw.lta 


 mri_convert -odt float -at /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/transforms/T2raw.lta -rl /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/orig/T2raw.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/T2.prenorm.mgz 


 mri_normalize -sigma 0.5 -nonmax_suppress 0 -min_dist 1 -aseg /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/aseg.presurf.mgz -surface /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/rh.white identity.nofile -surface /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/lh.white identity.nofile /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/T2.prenorm.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/T2.norm.mgz 


 mri_mask /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/T2.norm.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/brainmask.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/mri/T2.mgz 


 cp -v /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/lh.pial /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/lh.woT2.pial 


 mris_make_surfaces -orig_white white -orig_pial woT2.pial -aseg ../mri/aseg.presurf -nowhite -mgz -T1 brain.finalsurfs -T2 ../mri/T2 -nsigma_above 2 -nsigma_below 5 1257 lh 


 cp -v /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/rh.pial /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1257/surf/rh.woT2.pial 


 mris_make_surfaces -orig_white white -orig_pial woT2.pial -aseg ../mri/aseg.presurf -nowhite -mgz -T1 brain.finalsurfs -T2 ../mri/T2 -nsigma_above 2 -nsigma_below 5 1257 rh 

#--------------------------------------------
#@# Surf Volume lh Tue Apr 10 20:07:08 CEST 2018
#--------------------------------------------
#@# Surf Volume rh Tue Apr 10 20:07:11 CEST 2018
#--------------------------------------------
#@# Cortical ribbon mask Tue Apr 10 20:07:14 CEST 2018

 mris_volmask --aseg_name aseg.presurf --label_left_white 2 --label_left_ribbon 3 --label_right_white 41 --label_right_ribbon 42 --save_ribbon 1257 

#-----------------------------------------
#@# Parcellation Stats lh Tue Apr 10 20:13:45 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.stats -b -a ../label/lh.aparc.annot -c ../label/aparc.annot.ctab 1257 lh white 


 mris_anatomical_stats -th3 -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.pial.stats -b -a ../label/lh.aparc.annot -c ../label/aparc.annot.ctab 1257 lh pial 

#-----------------------------------------
#@# Parcellation Stats rh Tue Apr 10 20:13:45 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.stats -b -a ../label/rh.aparc.annot -c ../label/aparc.annot.ctab 1257 rh white 


 mris_anatomical_stats -th3 -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.pial.stats -b -a ../label/rh.aparc.annot -c ../label/aparc.annot.ctab 1257 rh pial 

#-----------------------------------------
#@# Cortical Parc 2 lh Tue Apr 10 20:14:20 CEST 2018

 mris_ca_label -l ../label/lh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 lh ../surf/lh.sphere.reg /opt/freesurfer/6.0.0/average/lh.CDaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/lh.aparc.a2009s.annot 

#-----------------------------------------
#@# Cortical Parc 2 rh Tue Apr 10 20:14:20 CEST 2018

 mris_ca_label -l ../label/rh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 rh ../surf/rh.sphere.reg /opt/freesurfer/6.0.0/average/rh.CDaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/rh.aparc.a2009s.annot 

#-----------------------------------------
#@# Parcellation Stats 2 lh Tue Apr 10 20:14:35 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.a2009s.stats -b -a ../label/lh.aparc.a2009s.annot -c ../label/aparc.annot.a2009s.ctab 1257 lh white 

#-----------------------------------------
#@# Parcellation Stats 2 rh Tue Apr 10 20:14:35 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.a2009s.stats -b -a ../label/rh.aparc.a2009s.annot -c ../label/aparc.annot.a2009s.ctab 1257 rh white 

#-----------------------------------------
#@# Cortical Parc 3 lh Tue Apr 10 20:15:05 CEST 2018

 mris_ca_label -l ../label/lh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 lh ../surf/lh.sphere.reg /opt/freesurfer/6.0.0/average/lh.DKTaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/lh.aparc.DKTatlas.annot 

#-----------------------------------------
#@# Cortical Parc 3 rh Tue Apr 10 20:15:05 CEST 2018

 mris_ca_label -l ../label/rh.cortex.label -aseg ../mri/aseg.presurf.mgz -seed 1234 1257 rh ../surf/rh.sphere.reg /opt/freesurfer/6.0.0/average/rh.DKTaparc.atlas.acfb40.noaparc.i12.2016-08-02.gcs ../label/rh.aparc.DKTatlas.annot 

#-----------------------------------------
#@# Parcellation Stats 3 lh Tue Apr 10 20:15:17 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.DKTatlas.stats -b -a ../label/lh.aparc.DKTatlas.annot -c ../label/aparc.annot.DKTatlas.ctab 1257 lh white 

#-----------------------------------------
#@# Parcellation Stats 3 rh Tue Apr 10 20:15:17 CEST 2018

 mris_anatomical_stats -th3 -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.DKTatlas.stats -b -a ../label/rh.aparc.DKTatlas.annot -c ../label/aparc.annot.DKTatlas.ctab 1257 rh white 

#-----------------------------------------
#@# WM/GM Contrast lh Tue Apr 10 20:15:47 CEST 2018

 pctsurfcon --s 1257 --lh-only 

#-----------------------------------------
#@# WM/GM Contrast rh Tue Apr 10 20:15:47 CEST 2018

 pctsurfcon --s 1257 --rh-only 

#-----------------------------------------
#@# Relabel Hypointensities Tue Apr 10 20:15:53 CEST 2018

 mri_relabel_hypointensities aseg.presurf.mgz ../surf aseg.presurf.hypos.mgz 

#-----------------------------------------
#@# AParc-to-ASeg aparc Tue Apr 10 20:16:09 CEST 2018

 mri_aparc2aseg --s 1257 --volmask --aseg aseg.presurf.hypos --relabel mri/norm.mgz mri/transforms/talairach.m3z /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca mri/aseg.auto_noCCseg.label_intensities.txt 

#-----------------------------------------
#@# AParc-to-ASeg a2009s Tue Apr 10 20:16:09 CEST 2018

 mri_aparc2aseg --s 1257 --volmask --aseg aseg.presurf.hypos --relabel mri/norm.mgz mri/transforms/talairach.m3z /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca mri/aseg.auto_noCCseg.label_intensities.txt --a2009s 

#-----------------------------------------
#@# AParc-to-ASeg DKTatlas Tue Apr 10 20:16:09 CEST 2018

 mri_aparc2aseg --s 1257 --volmask --aseg aseg.presurf.hypos --relabel mri/norm.mgz mri/transforms/talairach.m3z /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca mri/aseg.auto_noCCseg.label_intensities.txt --annot aparc.DKTatlas --o mri/aparc.DKTatlas+aseg.mgz 

#-----------------------------------------
#@# APas-to-ASeg Tue Apr 10 20:20:28 CEST 2018

 apas2aseg --i aparc+aseg.mgz --o aseg.mgz 

#--------------------------------------------
#@# ASeg Stats Tue Apr 10 20:20:33 CEST 2018

 mri_segstats --seg mri/aseg.mgz --sum stats/aseg.stats --pv mri/norm.mgz --empty --brainmask mri/brainmask.mgz --brain-vol-from-seg --excludeid 0 --excl-ctxgmwm --supratent --subcortgray --in mri/norm.mgz --in-intensity-name norm --in-intensity-units MR --etiv --surf-wm-vol --surf-ctx-vol --totalgray --euler --ctab /opt/freesurfer/6.0.0/ASegStatsLUT.txt --subject 1257 

#-----------------------------------------
#@# WMParc Tue Apr 10 20:21:17 CEST 2018

 mri_aparc2aseg --s 1257 --labelwm --hypo-as-wm --rip-unknown --volmask --o mri/wmparc.mgz --ctxseg aparc+aseg.mgz 


 mri_segstats --seg mri/wmparc.mgz --sum stats/wmparc.stats --pv mri/norm.mgz --excludeid 0 --brainmask mri/brainmask.mgz --in mri/norm.mgz --in-intensity-name norm --in-intensity-units MR --subject 1257 --surf-wm-vol --ctab /opt/freesurfer/6.0.0/WMParcStatsLUT.txt --etiv 

#--------------------------------------------
#@# BA_exvivo Labels lh Tue Apr 10 20:25:27 CEST 2018

 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA1_exvivo.label --trgsubject 1257 --trglabel ./lh.BA1_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA2_exvivo.label --trgsubject 1257 --trglabel ./lh.BA2_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA3a_exvivo.label --trgsubject 1257 --trglabel ./lh.BA3a_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA3b_exvivo.label --trgsubject 1257 --trglabel ./lh.BA3b_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA4a_exvivo.label --trgsubject 1257 --trglabel ./lh.BA4a_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA4p_exvivo.label --trgsubject 1257 --trglabel ./lh.BA4p_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA6_exvivo.label --trgsubject 1257 --trglabel ./lh.BA6_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA44_exvivo.label --trgsubject 1257 --trglabel ./lh.BA44_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA45_exvivo.label --trgsubject 1257 --trglabel ./lh.BA45_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.V1_exvivo.label --trgsubject 1257 --trglabel ./lh.V1_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.V2_exvivo.label --trgsubject 1257 --trglabel ./lh.V2_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.MT_exvivo.label --trgsubject 1257 --trglabel ./lh.MT_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.entorhinal_exvivo.label --trgsubject 1257 --trglabel ./lh.entorhinal_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.perirhinal_exvivo.label --trgsubject 1257 --trglabel ./lh.perirhinal_exvivo.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA1_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA1_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA2_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA2_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA3a_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA3a_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA3b_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA3b_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA4a_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA4a_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA4p_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA4p_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA6_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA6_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA44_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA44_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.BA45_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.BA45_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.V1_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.V1_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.V2_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.V2_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.MT_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.MT_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.entorhinal_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.entorhinal_exvivo.thresh.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/lh.perirhinal_exvivo.thresh.label --trgsubject 1257 --trglabel ./lh.perirhinal_exvivo.thresh.label --hemi lh --regmethod surface 


 mris_label2annot --s 1257 --hemi lh --ctab /opt/freesurfer/6.0.0/average/colortable_BA.txt --l lh.BA1_exvivo.label --l lh.BA2_exvivo.label --l lh.BA3a_exvivo.label --l lh.BA3b_exvivo.label --l lh.BA4a_exvivo.label --l lh.BA4p_exvivo.label --l lh.BA6_exvivo.label --l lh.BA44_exvivo.label --l lh.BA45_exvivo.label --l lh.V1_exvivo.label --l lh.V2_exvivo.label --l lh.MT_exvivo.label --l lh.entorhinal_exvivo.label --l lh.perirhinal_exvivo.label --a BA_exvivo --maxstatwinner --noverbose 


 mris_label2annot --s 1257 --hemi lh --ctab /opt/freesurfer/6.0.0/average/colortable_BA.txt --l lh.BA1_exvivo.thresh.label --l lh.BA2_exvivo.thresh.label --l lh.BA3a_exvivo.thresh.label --l lh.BA3b_exvivo.thresh.label --l lh.BA4a_exvivo.thresh.label --l lh.BA4p_exvivo.thresh.label --l lh.BA6_exvivo.thresh.label --l lh.BA44_exvivo.thresh.label --l lh.BA45_exvivo.thresh.label --l lh.V1_exvivo.thresh.label --l lh.V2_exvivo.thresh.label --l lh.MT_exvivo.thresh.label --l lh.entorhinal_exvivo.thresh.label --l lh.perirhinal_exvivo.thresh.label --a BA_exvivo.thresh --maxstatwinner --noverbose 


 mris_anatomical_stats -th3 -mgz -f ../stats/lh.BA_exvivo.stats -b -a ./lh.BA_exvivo.annot -c ./BA_exvivo.ctab 1257 lh white 


 mris_anatomical_stats -th3 -mgz -f ../stats/lh.BA_exvivo.thresh.stats -b -a ./lh.BA_exvivo.thresh.annot -c ./BA_exvivo.thresh.ctab 1257 lh white 

#--------------------------------------------
#@# BA_exvivo Labels rh Tue Apr 10 20:27:12 CEST 2018

 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA1_exvivo.label --trgsubject 1257 --trglabel ./rh.BA1_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA2_exvivo.label --trgsubject 1257 --trglabel ./rh.BA2_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA3a_exvivo.label --trgsubject 1257 --trglabel ./rh.BA3a_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA3b_exvivo.label --trgsubject 1257 --trglabel ./rh.BA3b_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA4a_exvivo.label --trgsubject 1257 --trglabel ./rh.BA4a_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA4p_exvivo.label --trgsubject 1257 --trglabel ./rh.BA4p_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA6_exvivo.label --trgsubject 1257 --trglabel ./rh.BA6_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA44_exvivo.label --trgsubject 1257 --trglabel ./rh.BA44_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA45_exvivo.label --trgsubject 1257 --trglabel ./rh.BA45_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.V1_exvivo.label --trgsubject 1257 --trglabel ./rh.V1_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.V2_exvivo.label --trgsubject 1257 --trglabel ./rh.V2_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.MT_exvivo.label --trgsubject 1257 --trglabel ./rh.MT_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.entorhinal_exvivo.label --trgsubject 1257 --trglabel ./rh.entorhinal_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.perirhinal_exvivo.label --trgsubject 1257 --trglabel ./rh.perirhinal_exvivo.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA1_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA1_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA2_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA2_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA3a_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA3a_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA3b_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA3b_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA4a_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA4a_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA4p_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA4p_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA6_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA6_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA44_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA44_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.BA45_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.BA45_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.V1_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.V1_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.V2_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.V2_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.MT_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.MT_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.entorhinal_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.entorhinal_exvivo.thresh.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/fsaverage/label/rh.perirhinal_exvivo.thresh.label --trgsubject 1257 --trglabel ./rh.perirhinal_exvivo.thresh.label --hemi rh --regmethod surface 


 mris_label2annot --s 1257 --hemi rh --ctab /opt/freesurfer/6.0.0/average/colortable_BA.txt --l rh.BA1_exvivo.label --l rh.BA2_exvivo.label --l rh.BA3a_exvivo.label --l rh.BA3b_exvivo.label --l rh.BA4a_exvivo.label --l rh.BA4p_exvivo.label --l rh.BA6_exvivo.label --l rh.BA44_exvivo.label --l rh.BA45_exvivo.label --l rh.V1_exvivo.label --l rh.V2_exvivo.label --l rh.MT_exvivo.label --l rh.entorhinal_exvivo.label --l rh.perirhinal_exvivo.label --a BA_exvivo --maxstatwinner --noverbose 


 mris_label2annot --s 1257 --hemi rh --ctab /opt/freesurfer/6.0.0/average/colortable_BA.txt --l rh.BA1_exvivo.thresh.label --l rh.BA2_exvivo.thresh.label --l rh.BA3a_exvivo.thresh.label --l rh.BA3b_exvivo.thresh.label --l rh.BA4a_exvivo.thresh.label --l rh.BA4p_exvivo.thresh.label --l rh.BA6_exvivo.thresh.label --l rh.BA44_exvivo.thresh.label --l rh.BA45_exvivo.thresh.label --l rh.V1_exvivo.thresh.label --l rh.V2_exvivo.thresh.label --l rh.MT_exvivo.thresh.label --l rh.entorhinal_exvivo.thresh.label --l rh.perirhinal_exvivo.thresh.label --a BA_exvivo.thresh --maxstatwinner --noverbose 


 mris_anatomical_stats -th3 -mgz -f ../stats/rh.BA_exvivo.stats -b -a ./rh.BA_exvivo.annot -c ./BA_exvivo.ctab 1257 rh white 


 mris_anatomical_stats -th3 -mgz -f ../stats/rh.BA_exvivo.thresh.stats -b -a ./rh.BA_exvivo.thresh.annot -c ./BA_exvivo.thresh.ctab 1257 rh white 

