

#---------------------------------
# New invocation of recon-all Tue Apr 10 14:20:06 CEST 2018 

 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD2258_T1w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/2258/mri/orig/001.mgz 

#--------------------------------------------
#@# T2/FLAIR Input Tue Apr 10 14:21:47 CEST 2018

 mri_convert --no_scale 1 /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD2258_T2w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/2258/mri/orig/T2raw.mgz 

