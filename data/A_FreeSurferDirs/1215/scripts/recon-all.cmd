

#---------------------------------
# New invocation of recon-all Tue Apr 10 14:19:25 CEST 2018 

 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1215_T1w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig/001.mgz 

#--------------------------------------------
#@# T2/FLAIR Input Tue Apr 10 14:20:10 CEST 2018

 mri_convert --no_scale 1 /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1215_T2w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig/T2raw.mgz 

#--------------------------------------------
#@# MotionCor Tue Apr 10 14:21:20 CEST 2018

 cp /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig/001.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/rawavg.mgz 


 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/rawavg.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig.mgz --conform 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/transforms/talairach.xfm /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/orig.mgz 

#--------------------------------------------
#@# Talairach Tue Apr 10 14:22:43 CEST 2018

 mri_nu_correct.mni --no-rescale --i orig.mgz --o orig_nu.mgz --n 1 --proto-iters 1000 --distance 50 


 talairach_avi --i orig_nu.mgz --xfm transforms/talairach.auto.xfm 

talairach_avi log file is transforms/talairach_avi.log...

 cp transforms/talairach.auto.xfm transforms/talairach.xfm 

#--------------------------------------------
#@# Talairach Failure Detection Tue Apr 10 14:30:10 CEST 2018

 talairach_afd -T 0.005 -xfm transforms/talairach.xfm 


 awk -f /opt/freesurfer/6.0.0/bin/extract_talairach_avi_QA.awk /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/transforms/talairach_avi.log 


 tal_QC_AZS /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/transforms/talairach_avi.log 

#--------------------------------------------
#@# Nu Intensity Correction Tue Apr 10 14:30:12 CEST 2018

 mri_nu_correct.mni --i orig.mgz --o nu.mgz --uchar transforms/talairach.xfm --n 2 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/transforms/talairach.xfm nu.mgz nu.mgz 

#--------------------------------------------
#@# Intensity Normalization Tue Apr 10 14:43:35 CEST 2018

 mri_normalize -g 1 -mprage nu.mgz T1.mgz 

#--------------------------------------------
#@# Skull Stripping Tue Apr 10 14:45:49 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mri_em_register.skull.dat -skull nu.mgz /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta 


 mri_watershed -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mri_watershed.dat -T1 -brain_atlas /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta T1.mgz brainmask.auto.mgz 


 cp brainmask.auto.mgz brainmask.mgz 

#-------------------------------------
#@# EM Registration Tue Apr 10 14:50:04 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mri_em_register.dat -uns 3 -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta 

#--------------------------------------
#@# CA Normalize Tue Apr 10 14:53:42 CEST 2018

 mri_ca_normalize -c ctrl_pts.mgz -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta norm.mgz 

#--------------------------------------
#@# CA Reg Tue Apr 10 14:55:14 CEST 2018

 mri_ca_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mri_ca_register.dat -nobigventricles -T transforms/talairach.lta -align-after -mask brainmask.mgz norm.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.m3z 

#--------------------------------------
#@# SubCort Seg Tue Apr 10 16:24:40 CEST 2018

 mri_ca_label -relabel_unlikely 9 .3 -prior 0.5 -align norm.mgz transforms/talairach.m3z /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca aseg.auto_noCCseg.mgz 


 mri_cc -aseg aseg.auto_noCCseg.mgz -o aseg.auto.mgz -lta /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/mri/transforms/cc_up.lta 1215 

#--------------------------------------
#@# Merge ASeg Tue Apr 10 17:13:00 CEST 2018

 cp aseg.auto.mgz aseg.presurf.mgz 

#--------------------------------------------
#@# Intensity Normalization2 Tue Apr 10 17:13:00 CEST 2018

 mri_normalize -mprage -aseg aseg.presurf.mgz -mask brainmask.mgz norm.mgz brain.mgz 

#--------------------------------------------
#@# Mask BFS Tue Apr 10 17:14:54 CEST 2018

 mri_mask -T 5 brain.mgz brainmask.mgz brain.finalsurfs.mgz 

#--------------------------------------------
#@# WM Segmentation Tue Apr 10 17:14:55 CEST 2018

 mri_segment -mprage brain.mgz wm.seg.mgz 


 mri_edit_wm_with_aseg -keep-in wm.seg.mgz brain.mgz aseg.presurf.mgz wm.asegedit.mgz 


 mri_pretess wm.asegedit.mgz wm norm.mgz wm.mgz 

#--------------------------------------------
#@# Fill Tue Apr 10 17:16:55 CEST 2018

 mri_fill -a ../scripts/ponscc.cut.log -xform transforms/talairach.lta -segmentation aseg.auto_noCCseg.mgz wm.mgz filled.mgz 

#--------------------------------------------
#@# Tessellate lh Tue Apr 10 17:17:23 CEST 2018

 mri_pretess ../mri/filled.mgz 255 ../mri/norm.mgz ../mri/filled-pretess255.mgz 


 mri_tessellate ../mri/filled-pretess255.mgz 255 ../surf/lh.orig.nofix 


 rm -f ../mri/filled-pretess255.mgz 


 mris_extract_main_component ../surf/lh.orig.nofix ../surf/lh.orig.nofix 

#--------------------------------------------
#@# Tessellate rh Tue Apr 10 17:17:29 CEST 2018

 mri_pretess ../mri/filled.mgz 127 ../mri/norm.mgz ../mri/filled-pretess127.mgz 


 mri_tessellate ../mri/filled-pretess127.mgz 127 ../surf/rh.orig.nofix 


 rm -f ../mri/filled-pretess127.mgz 


 mris_extract_main_component ../surf/rh.orig.nofix ../surf/rh.orig.nofix 

#--------------------------------------------
#@# Smooth1 lh Tue Apr 10 17:17:34 CEST 2018

 mris_smooth -nw -seed 1234 ../surf/lh.orig.nofix ../surf/lh.smoothwm.nofix 

#--------------------------------------------
#@# Smooth1 rh Tue Apr 10 17:17:35 CEST 2018

 mris_smooth -nw -seed 1234 ../surf/rh.orig.nofix ../surf/rh.smoothwm.nofix 

#--------------------------------------------
#@# Inflation1 lh Tue Apr 10 17:17:42 CEST 2018

 mris_inflate -no-save-sulc ../surf/lh.smoothwm.nofix ../surf/lh.inflated.nofix 

#--------------------------------------------
#@# Inflation1 rh Tue Apr 10 17:17:42 CEST 2018

 mris_inflate -no-save-sulc ../surf/rh.smoothwm.nofix ../surf/rh.inflated.nofix 

#--------------------------------------------
#@# QSphere lh Tue Apr 10 17:18:22 CEST 2018

 mris_sphere -q -seed 1234 ../surf/lh.inflated.nofix ../surf/lh.qsphere.nofix 

#--------------------------------------------
#@# QSphere rh Tue Apr 10 17:18:22 CEST 2018

 mris_sphere -q -seed 1234 ../surf/rh.inflated.nofix ../surf/rh.qsphere.nofix 

#--------------------------------------------
#@# Fix Topology Copy lh Tue Apr 10 17:22:28 CEST 2018

 cp ../surf/lh.orig.nofix ../surf/lh.orig 


 cp ../surf/lh.inflated.nofix ../surf/lh.inflated 

#--------------------------------------------
#@# Fix Topology Copy rh Tue Apr 10 17:22:29 CEST 2018

 cp ../surf/rh.orig.nofix ../surf/rh.orig 


 cp ../surf/rh.inflated.nofix ../surf/rh.inflated 

#@# Fix Topology lh Tue Apr 10 17:22:29 CEST 2018

 mris_fix_topology -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mris_fix_topology.lh.dat -mgz -sphere qsphere.nofix -ga -seed 1234 1215 lh 

#@# Fix Topology rh Tue Apr 10 17:22:29 CEST 2018

 mris_fix_topology -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1215/touch/rusage.mris_fix_topology.rh.dat -mgz -sphere qsphere.nofix -ga -seed 1234 1215 rh 

