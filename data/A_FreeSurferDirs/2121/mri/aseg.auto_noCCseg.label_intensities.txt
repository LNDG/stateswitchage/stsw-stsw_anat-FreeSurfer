1 Left_Cerebral_Exterior 1.44 0.0  0
2 Left_Cerebral_White_Matter 0.99 0.0 103
3 Left_Cerebral_Cortex 1.31 0.0 76
4 Left_Lateral_Ventricle 1.24 0.0 23
5 Left_Inf_Lat_Vent 1.26 0.0 40
7 Left_Cerebellum_White_Matter 1.08 0.0 91
8 Left_Cerebellum_Cortex 1.29 0.0 72
9 Left_Thalamus 1.30 0.0 101
10 Left_Thalamus_Proper 1.03 0.0 93
11 Left_Caudate 1.09 0.0 84
12 Left_Putamen 1.08 0.0 81
13 Left_Pallidum 1.01 0.0 96
14 Third_Ventricle 1.13 0.0 29
15 Fourth_Ventricle 1.49 0.0 22
16 Brain_Stem 0.99 0.0 78
17 Left_Hippocampus 1.26 0.0 71
18 Left_Amygdala 1.27 0.0 74
24 CSF 1.44 0.0 53
26 Left_Accumbens_area 1.09 0.0 68
28 Left_VentralDC 1.01 0.0 87
40 Right_Cerebral_Exterior 1.44 0.0  0
41 Right_Cerebral_White_Matter 0.99 0.0 101
42 Right_Cerebral_Cortex 1.31 0.0 76
43 Right_Lateral_Ventricle 1.94 0.0 25
44 Right_Inf_Lat_Vent 1.33 0.0 30
46 Right_Cerebellum_White_Matter 1.03 0.0 87
47 Right_Cerebellum_Cortex 1.33 0.0 73
48 Right_Thalamus 1.30 0.0  0
49 Right_Thalamus_Proper 1.10 0.0 91
50 Right_Caudate 1.17 0.0 78
51 Right_Putamen 1.08 0.0 81
52 Right_Pallidum 0.98 0.0 93
53 Right_Hippocampus 1.33 0.0 74
54 Right_Amygdala 1.29 0.0 74
58 Right_Accumbens_area 1.17 0.0 77
60 Right_VentralDC 1.02 0.0 90
72 Fifth_Ventricle 1.44 0.0 44
75  1.44 0.0  0
76  1.44 0.0  0
77 WM_hypointensities 0.99 0.0 76
78 Left_WM_hypointensities 0.99 0.0  0
79 Right_WM_hypointensities 0.99 0.0  0
80 non_WM_hypointensities 0.99 0.0 54
81 Left_non_WM_hypointensities 0.99 0.0  0
82 Right_non_WM_hypointensities 0.99 0.0  0
186  0.99 0.0  0
187  0.99 0.0  0
