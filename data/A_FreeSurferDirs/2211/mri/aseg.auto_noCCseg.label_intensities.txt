1 Left_Cerebral_Exterior 1.40 0.0  0
2 Left_Cerebral_White_Matter 0.99 0.0 103
3 Left_Cerebral_Cortex 1.31 0.0 76
4 Left_Lateral_Ventricle 1.29 0.0 24
5 Left_Inf_Lat_Vent 1.21 0.0 33
7 Left_Cerebellum_White_Matter 1.08 0.0 91
8 Left_Cerebellum_Cortex 1.26 0.0 70
9 Left_Thalamus 1.29 0.0 102
10 Left_Thalamus_Proper 1.04 0.0 94
11 Left_Caudate 1.07 0.0 73
12 Left_Putamen 1.10 0.0 89
13 Left_Pallidum 1.04 0.0 99
14 Third_Ventricle 1.05 0.0 27
15 Fourth_Ventricle 1.50 0.0 22
16 Brain_Stem 1.05 0.0 83
17 Left_Hippocampus 1.21 0.0 68
18 Left_Amygdala 1.32 0.0 77
24 CSF 1.40 0.0 51
26 Left_Accumbens_area 1.07 0.0 67
28 Left_VentralDC 1.12 0.0 96
40 Right_Cerebral_Exterior 1.40 0.0  0
41 Right_Cerebral_White_Matter 0.99 0.0 101
42 Right_Cerebral_Cortex 1.31 0.0 76
43 Right_Lateral_Ventricle 1.82 0.0 23
44 Right_Inf_Lat_Vent 1.32 0.0 30
46 Right_Cerebellum_White_Matter 1.03 0.0 87
47 Right_Cerebellum_Cortex 1.30 0.0 72
48 Right_Thalamus 1.29 0.0  0
49 Right_Thalamus_Proper 1.10 0.0 89
50 Right_Caudate 1.19 0.0 79
51 Right_Putamen 1.08 0.0 86
52 Right_Pallidum 1.06 0.0 99
53 Right_Hippocampus 1.32 0.0 73
54 Right_Amygdala 1.28 0.0 73
58 Right_Accumbens_area 1.19 0.0 77
60 Right_VentralDC 1.13 0.0 100
72 Fifth_Ventricle 1.40 0.0 44
75  1.40 0.0  0
76  1.40 0.0  0
77 WM_hypointensities 0.99 0.0 76
78 Left_WM_hypointensities 0.99 0.0  0
79 Right_WM_hypointensities 0.99 0.0  0
80 non_WM_hypointensities 0.99 0.0 54
81 Left_non_WM_hypointensities 0.99 0.0  0
82 Right_non_WM_hypointensities 0.99 0.0  0
186  0.99 0.0  0
187  0.99 0.0  0
