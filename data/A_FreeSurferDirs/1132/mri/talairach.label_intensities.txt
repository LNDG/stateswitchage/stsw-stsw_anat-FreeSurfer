1 Left_Cerebral_Exterior 0.67 0.0  0
2 Left_Cerebral_White_Matter 1.00 0.0 102
3 Left_Cerebral_Cortex 1.20 0.0 71
4 Left_Lateral_Ventricle 0.60 0.0 12
5 Left_Inf_Lat_Vent 1.18 0.0 38
7 Left_Cerebellum_White_Matter 1.05 0.0 89
8 Left_Cerebellum_Cortex 1.35 0.0 77
9 Left_Thalamus 1.21 0.0 114
10 Left_Thalamus_Proper 1.07 0.0 91
11 Left_Caudate 1.03 0.0 73
12 Left_Putamen 1.04 0.0 83
13 Left_Pallidum 0.95 0.0 89
14 Third_Ventricle 0.67 0.0 15
15 Fourth_Ventricle 0.68 0.0 14
16 Brain_Stem 1.05 0.0 84
17 Left_Hippocampus 1.18 0.0 69
18 Left_Amygdala 1.24 0.0 70
24 CSF 0.67 0.0 23
26 Left_Accumbens_area 1.03 0.0 64
28 Left_VentralDC 1.03 0.0 92
40 Right_Cerebral_Exterior 0.67 0.0  0
41 Right_Cerebral_White_Matter 1.01 0.0 103
42 Right_Cerebral_Cortex 1.16 0.0 68
43 Right_Lateral_Ventricle 0.74 0.0 12
44 Right_Inf_Lat_Vent 1.23 0.0 32
46 Right_Cerebellum_White_Matter 1.02 0.0 85
47 Right_Cerebellum_Cortex 1.30 0.0 73
48 Right_Thalamus 1.21 0.0  0
49 Right_Thalamus_Proper 1.11 0.0 92
50 Right_Caudate 1.08 0.0 72
51 Right_Putamen 1.10 0.0 87
52 Right_Pallidum 0.95 0.0 92
53 Right_Hippocampus 1.23 0.0 67
54 Right_Amygdala 1.27 0.0 71
58 Right_Accumbens_area 1.08 0.0 69
60 Right_VentralDC 1.04 0.0 90
72 Fifth_Ventricle 0.67 0.0 24
75  0.67 0.0  0
76  0.67 0.0  0
77 WM_hypointensities 1.01 0.0 77
78 Left_WM_hypointensities 1.01 0.0  0
79 Right_WM_hypointensities 1.01 0.0  0
80 non_WM_hypointensities 1.01 0.0 47
81 Left_non_WM_hypointensities 1.01 0.0  0
82 Right_non_WM_hypointensities 1.01 0.0  0
186  1.01 0.0  0
187  1.01 0.0  0
