1 Left_Cerebral_Exterior 0.94 0.0  0
2 Left_Cerebral_White_Matter 1.00 0.0 104
3 Left_Cerebral_Cortex 1.26 0.0 72
4 Left_Lateral_Ventricle 0.73 0.0 14
5 Left_Inf_Lat_Vent 1.18 0.0 32
7 Left_Cerebellum_White_Matter 1.02 0.0 86
8 Left_Cerebellum_Cortex 1.26 0.0 70
9 Left_Thalamus 1.23 0.0 103
10 Left_Thalamus_Proper 1.04 0.0 94
11 Left_Caudate 1.11 0.0 86
12 Left_Putamen 1.14 0.0 93
13 Left_Pallidum 1.04 0.0 100
14 Third_Ventricle 0.94 0.0 24
15 Fourth_Ventricle 1.00 0.0 15
16 Brain_Stem 1.00 0.0 78
17 Left_Hippocampus 1.18 0.0 70
18 Left_Amygdala 1.28 0.0 74
24 CSF 0.94 0.0 34
26 Left_Accumbens_area 1.11 0.0 69
28 Left_VentralDC 1.01 0.0 88
40 Right_Cerebral_Exterior 0.94 0.0  0
41 Right_Cerebral_White_Matter 1.00 0.0 102
42 Right_Cerebral_Cortex 1.18 0.0 68
43 Right_Lateral_Ventricle 1.07 0.0 13
44 Right_Inf_Lat_Vent 1.17 0.0 27
46 Right_Cerebellum_White_Matter 0.94 0.0 79
47 Right_Cerebellum_Cortex 1.21 0.0 67
48 Right_Thalamus 1.23 0.0  0
49 Right_Thalamus_Proper 1.07 0.0 86
50 Right_Caudate 1.17 0.0 78
51 Right_Putamen 1.11 0.0 87
52 Right_Pallidum 0.99 0.0 93
53 Right_Hippocampus 1.17 0.0 65
54 Right_Amygdala 1.32 0.0 75
58 Right_Accumbens_area 1.17 0.0 76
60 Right_VentralDC 1.04 0.0 93
72 Fifth_Ventricle 0.94 0.0 30
75  0.94 0.0  0
76  0.94 0.0  0
77 WM_hypointensities 1.00 0.0 76
78 Left_WM_hypointensities 1.00 0.0  0
79 Right_WM_hypointensities 1.00 0.0  0
80 non_WM_hypointensities 1.00 0.0 43
81 Left_non_WM_hypointensities 1.00 0.0  0
82 Right_non_WM_hypointensities 1.00 0.0  0
186  1.00 0.0  0
187  1.00 0.0  0
