1 Left_Cerebral_Exterior 0.96 0.0  0
2 Left_Cerebral_White_Matter 0.99 0.0 103
3 Left_Cerebral_Cortex 1.14 0.0 66
4 Left_Lateral_Ventricle 0.79 0.0 14
5 Left_Inf_Lat_Vent 1.12 0.0 30
7 Left_Cerebellum_White_Matter 1.04 0.0 88
8 Left_Cerebellum_Cortex 1.32 0.0 73
9 Left_Thalamus 1.12 0.0 100
10 Left_Thalamus_Proper 1.00 0.0 89
11 Left_Caudate 1.08 0.0 74
12 Left_Putamen 1.14 0.0 92
13 Left_Pallidum 1.04 0.0 99
14 Third_Ventricle 0.96 0.0 25
15 Fourth_Ventricle 0.93 0.0 13
16 Brain_Stem 0.99 0.0 78
17 Left_Hippocampus 1.12 0.0 63
18 Left_Amygdala 1.17 0.0 68
24 CSF 0.96 0.0 35
26 Left_Accumbens_area 1.08 0.0 67
28 Left_VentralDC 1.00 0.0 85
40 Right_Cerebral_Exterior 0.96 0.0  0
41 Right_Cerebral_White_Matter 0.99 0.0 101
42 Right_Cerebral_Cortex 1.13 0.0 65
43 Right_Lateral_Ventricle 1.15 0.0 14
44 Right_Inf_Lat_Vent 1.08 0.0 25
46 Right_Cerebellum_White_Matter 0.96 0.0 81
47 Right_Cerebellum_Cortex 1.22 0.0 67
48 Right_Thalamus 1.12 0.0  0
49 Right_Thalamus_Proper 1.00 0.0 83
50 Right_Caudate 1.19 0.0 79
51 Right_Putamen 1.09 0.0 85
52 Right_Pallidum 0.98 0.0 91
53 Right_Hippocampus 1.08 0.0 60
54 Right_Amygdala 1.10 0.0 63
58 Right_Accumbens_area 1.19 0.0 77
60 Right_VentralDC 0.96 0.0 86
72 Fifth_Ventricle 0.96 0.0 31
75  0.96 0.0  0
76  0.96 0.0  0
77 WM_hypointensities 0.99 0.0 75
78 Left_WM_hypointensities 0.99 0.0  0
79 Right_WM_hypointensities 0.99 0.0  0
80 non_WM_hypointensities 0.99 0.0 54
81 Left_non_WM_hypointensities 0.99 0.0  0
82 Right_non_WM_hypointensities 0.99 0.0  0
186  0.99 0.0  0
187  0.99 0.0  0
