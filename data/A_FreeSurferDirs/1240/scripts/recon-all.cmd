

#---------------------------------
# New invocation of recon-all Tue Apr 10 14:19:26 CEST 2018 

 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1240_T1w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1240/mri/orig/001.mgz 

#--------------------------------------------
#@# T2/FLAIR Input Tue Apr 10 14:20:31 CEST 2018

 mri_convert --no_scale 1 /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1240_T2w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1240/mri/orig/T2raw.mgz 

