1 Left_Cerebral_Exterior 1.43 0.0  0
2 Left_Cerebral_White_Matter 1.00 0.0 104
3 Left_Cerebral_Cortex 1.41 0.0 82
4 Left_Lateral_Ventricle 1.26 0.0 26
5 Left_Inf_Lat_Vent 1.28 0.0 34
7 Left_Cerebellum_White_Matter 1.10 0.0 93
8 Left_Cerebellum_Cortex 1.35 0.0 75
9 Left_Thalamus 1.35 0.0 104
10 Left_Thalamus_Proper 1.10 0.0 97
11 Left_Caudate 1.24 0.0 96
12 Left_Putamen 1.11 0.0 92
13 Left_Pallidum 1.04 0.0 100
14 Third_Ventricle 1.13 0.0 29
15 Fourth_Ventricle 1.59 0.0 24
16 Brain_Stem 1.12 0.0 88
17 Left_Hippocampus 1.28 0.0 72
18 Left_Amygdala 1.35 0.0 78
24 CSF 1.43 0.0 52
26 Left_Accumbens_area 1.24 0.0 77
28 Left_VentralDC 1.16 0.0 99
40 Right_Cerebral_Exterior 1.43 0.0  0
41 Right_Cerebral_White_Matter 1.00 0.0 102
42 Right_Cerebral_Cortex 1.41 0.0 82
43 Right_Lateral_Ventricle 1.78 0.0 23
44 Right_Inf_Lat_Vent 1.31 0.0 31
46 Right_Cerebellum_White_Matter 1.10 0.0 92
47 Right_Cerebellum_Cortex 1.35 0.0 74
48 Right_Thalamus 1.35 0.0  0
49 Right_Thalamus_Proper 1.12 0.0 90
50 Right_Caudate 1.24 0.0 81
51 Right_Putamen 1.09 0.0 84
52 Right_Pallidum 1.04 0.0 100
53 Right_Hippocampus 1.31 0.0 73
54 Right_Amygdala 1.36 0.0 77
58 Right_Accumbens_area 1.24 0.0 80
60 Right_VentralDC 1.13 0.0 100
72 Fifth_Ventricle 1.43 0.0 45
75  1.43 0.0  0
76  1.43 0.0  0
77 WM_hypointensities 1.00 0.0 76
78 Left_WM_hypointensities 1.00 0.0  0
79 Right_WM_hypointensities 1.00 0.0  0
80 non_WM_hypointensities 1.00 0.0 43
81 Left_non_WM_hypointensities 1.00 0.0  0
82 Right_non_WM_hypointensities 1.00 0.0  0
186  1.00 0.0  0
187  1.00 0.0  0
