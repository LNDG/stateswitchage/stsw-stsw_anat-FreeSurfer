1 Left_Cerebral_Exterior 1.00 0.0  0
2 Left_Cerebral_White_Matter 0.99 0.0 103
3 Left_Cerebral_Cortex 1.20 0.0 69
4 Left_Lateral_Ventricle 0.78 0.0 14
5 Left_Inf_Lat_Vent 1.17 0.0 38
7 Left_Cerebellum_White_Matter 1.07 0.0 90
8 Left_Cerebellum_Cortex 1.31 0.0 73
9 Left_Thalamus 1.21 0.0 101
10 Left_Thalamus_Proper 1.08 0.0 95
11 Left_Caudate 1.12 0.0 87
12 Left_Putamen 1.14 0.0 92
13 Left_Pallidum 1.04 0.0 99
14 Third_Ventricle 1.00 0.0 26
15 Fourth_Ventricle 1.05 0.0 15
16 Brain_Stem 0.99 0.0 78
17 Left_Hippocampus 1.17 0.0 66
18 Left_Amygdala 1.25 0.0 71
24 CSF 1.00 0.0 36
26 Left_Accumbens_area 1.12 0.0 70
28 Left_VentralDC 1.07 0.0 91
40 Right_Cerebral_Exterior 1.00 0.0  0
41 Right_Cerebral_White_Matter 0.99 0.0 101
42 Right_Cerebral_Cortex 1.24 0.0 71
43 Right_Lateral_Ventricle 1.15 0.0 14
44 Right_Inf_Lat_Vent 1.21 0.0 28
46 Right_Cerebellum_White_Matter 1.08 0.0 90
47 Right_Cerebellum_Cortex 1.35 0.0 74
48 Right_Thalamus 1.21 0.0  0
49 Right_Thalamus_Proper 1.11 0.0 89
50 Right_Caudate 1.16 0.0 77
51 Right_Putamen 1.11 0.0 87
52 Right_Pallidum 1.02 0.0 98
53 Right_Hippocampus 1.21 0.0 67
54 Right_Amygdala 1.20 0.0 69
58 Right_Accumbens_area 1.16 0.0 75
60 Right_VentralDC 1.07 0.0 95
72 Fifth_Ventricle 1.00 0.0 32
75  1.00 0.0  0
76  1.00 0.0  0
77 WM_hypointensities 0.99 0.0 76
78 Left_WM_hypointensities 0.99 0.0  0
79 Right_WM_hypointensities 0.99 0.0  0
80 non_WM_hypointensities 0.99 0.0 54
81 Left_non_WM_hypointensities 0.99 0.0  0
82 Right_non_WM_hypointensities 0.99 0.0  0
186  0.99 0.0  0
187  0.99 0.0  0
