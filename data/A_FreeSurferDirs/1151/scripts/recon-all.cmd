

#---------------------------------
# New invocation of recon-all Tue Apr 10 14:19:24 CEST 2018 

 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1151_T1w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig/001.mgz 

#--------------------------------------------
#@# T2/FLAIR Input Tue Apr 10 14:19:57 CEST 2018

 mri_convert --no_scale 1 /home/mpib/LNDG/StateSwitch/WIP_anat/preproc/B_data/B_nii/sub-STSWD1151_T2w.nii.gz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig/T2raw.mgz 

#--------------------------------------------
#@# MotionCor Tue Apr 10 14:21:25 CEST 2018

 cp /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig/001.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/rawavg.mgz 


 mri_convert /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/rawavg.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig.mgz --conform 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/transforms/talairach.xfm /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig.mgz /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/orig.mgz 

#--------------------------------------------
#@# Talairach Tue Apr 10 14:22:49 CEST 2018

 mri_nu_correct.mni --no-rescale --i orig.mgz --o orig_nu.mgz --n 1 --proto-iters 1000 --distance 50 


 talairach_avi --i orig_nu.mgz --xfm transforms/talairach.auto.xfm 

talairach_avi log file is transforms/talairach_avi.log...

 cp transforms/talairach.auto.xfm transforms/talairach.xfm 

#--------------------------------------------
#@# Talairach Failure Detection Tue Apr 10 14:35:56 CEST 2018

 talairach_afd -T 0.005 -xfm transforms/talairach.xfm 


 awk -f /opt/freesurfer/6.0.0/bin/extract_talairach_avi_QA.awk /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/transforms/talairach_avi.log 


 tal_QC_AZS /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/transforms/talairach_avi.log 

#--------------------------------------------
#@# Nu Intensity Correction Tue Apr 10 14:36:01 CEST 2018

 mri_nu_correct.mni --i orig.mgz --o nu.mgz --uchar transforms/talairach.xfm --n 2 


 mri_add_xform_to_header -c /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/mri/transforms/talairach.xfm nu.mgz nu.mgz 

#--------------------------------------------
#@# Intensity Normalization Tue Apr 10 14:54:13 CEST 2018

 mri_normalize -g 1 -mprage nu.mgz T1.mgz 

#--------------------------------------------
#@# Skull Stripping Tue Apr 10 14:56:54 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/touch/rusage.mri_em_register.skull.dat -skull nu.mgz /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta 


 mri_watershed -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/touch/rusage.mri_watershed.dat -T1 -brain_atlas /opt/freesurfer/6.0.0/average/RB_all_withskull_2016-05-10.vc700.gca transforms/talairach_with_skull.lta T1.mgz brainmask.auto.mgz 


 cp brainmask.auto.mgz brainmask.mgz 

#-------------------------------------
#@# EM Registration Tue Apr 10 15:01:30 CEST 2018

 mri_em_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/touch/rusage.mri_em_register.dat -uns 3 -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta 

#--------------------------------------
#@# CA Normalize Tue Apr 10 15:05:40 CEST 2018

 mri_ca_normalize -c ctrl_pts.mgz -mask brainmask.mgz nu.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.lta norm.mgz 

#--------------------------------------
#@# CA Reg Tue Apr 10 15:07:43 CEST 2018

 mri_ca_register -rusage /home/mpib/LNDG/StateSwitch/WIP_anat/analyses/A_FreeSurfer/B_data/A_FreeSurferDirs/1151/touch/rusage.mri_ca_register.dat -nobigventricles -T transforms/talairach.lta -align-after -mask brainmask.mgz norm.mgz /opt/freesurfer/6.0.0/average/RB_all_2016-05-10.vc700.gca transforms/talairach.m3z 

