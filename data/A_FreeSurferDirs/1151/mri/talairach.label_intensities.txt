1 Left_Cerebral_Exterior 1.40 0.0  0
2 Left_Cerebral_White_Matter 1.01 0.0 104
3 Left_Cerebral_Cortex 1.25 0.0 73
4 Left_Lateral_Ventricle 1.43 0.0 29
5 Left_Inf_Lat_Vent 1.23 0.0 39
7 Left_Cerebellum_White_Matter 1.04 0.0 88
8 Left_Cerebellum_Cortex 1.26 0.0 72
9 Left_Thalamus 1.21 0.0 114
10 Left_Thalamus_Proper 1.13 0.0 96
11 Left_Caudate 1.15 0.0 82
12 Left_Putamen 1.15 0.0 91
13 Left_Pallidum 1.07 0.0 99
14 Third_Ventricle 1.40 0.0 32
15 Fourth_Ventricle 1.30 0.0 27
16 Brain_Stem 1.02 0.0 82
17 Left_Hippocampus 1.23 0.0 71
18 Left_Amygdala 1.28 0.0 73
24 CSF 1.40 0.0 48
26 Left_Accumbens_area 1.15 0.0 72
28 Left_VentralDC 1.04 0.0 93
40 Right_Cerebral_Exterior 1.40 0.0  0
41 Right_Cerebral_White_Matter 1.01 0.0 103
42 Right_Cerebral_Cortex 1.23 0.0 71
43 Right_Lateral_Ventricle 1.47 0.0 23
44 Right_Inf_Lat_Vent 1.15 0.0 30
46 Right_Cerebellum_White_Matter 0.94 0.0 78
47 Right_Cerebellum_Cortex 1.21 0.0 67
48 Right_Thalamus 1.21 0.0  0
49 Right_Thalamus_Proper 1.07 0.0 88
50 Right_Caudate 1.20 0.0 80
51 Right_Putamen 1.14 0.0 90
52 Right_Pallidum 1.00 0.0 96
53 Right_Hippocampus 1.15 0.0 64
54 Right_Amygdala 1.13 0.0 64
58 Right_Accumbens_area 1.20 0.0 76
60 Right_VentralDC 1.02 0.0 88
72 Fifth_Ventricle 1.40 0.0 50
75  1.40 0.0  0
76  1.40 0.0  0
77 WM_hypointensities 1.01 0.0 77
78 Left_WM_hypointensities 1.01 0.0  0
79 Right_WM_hypointensities 1.01 0.0  0
80 non_WM_hypointensities 1.01 0.0 48
81 Left_non_WM_hypointensities 1.01 0.0  0
82 Right_non_WM_hypointensities 1.01 0.0  0
186  1.01 0.0  0
187  1.01 0.0  0
